extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (String) var scene = "Level 2"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if('Player' in body.get_groups()):
		Global.startLevelPlayer1CurrHealth = Global.player1CurrHealth
		Global.startLevelPlayer1MaxHealth = Global.player1MaxHealth
		Global.startLevelPlayer2CurrHealth = Global.player2CurrHealth
		Global.startLevelPlayer2MaxHealth = Global.player2MaxHealth
		Global.startLevelPlayerScore = Global.playerCurrScore
		Global.goto_scene("res://Scenes/" + scene + ".tscn")
		
