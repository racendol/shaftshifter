extends CanvasLayer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	setHealthMaxValue(Global.currentPlayer)
	if !Global.player2Unlocked:
		$PlayerList.visible = false

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	setHealthCurrentValue(Global.currentPlayer)
	if !Global.player1Available or Global.currentPlayer == "Player":
		if Global.player1CurrHealth > 0:
			$PlayerList.get_node("Player1Control").modulate = Color(0.5, 0.5, 0.5, 1)
		else:
			$PlayerList.get_node("Player1Control").modulate = Color(0, 0, 0, 1)
	elif Global.player1CurrHealth > 0:
		$PlayerList.get_node("Player1Control").modulate = Color(1, 1, 1, 1)
		
	if !Global.player2Available or Global.currentPlayer == "Player2":
		if Global.player2CurrHealth:
			$PlayerList.get_node("Player2Control").modulate = Color(0.5, 0.5, 0.5, 1)
		else:
			$PlayerList.get_node("Player2Control").modulate = Color(0, 0, 0, 1)
	elif Global.player2CurrHealth > 0:
		$PlayerList.get_node("Player2Control").modulate = Color(1, 1, 1, 1)
	
	setScore()

func setScore():
	var scoreString = str(Global.playerCurrScore)
	var zeroLength = 6 - scoreString.length()
	var zeros = ''
	for i in range(zeroLength):
		zeros += '0'
	var score = zeros + scoreString
	$ScoreText.text = score
	$StageClearPanel/ScoreText.text = score
		
func setHealthMaxValue(player):
	if Global.currentPlayer == "Player":
		$PlayerHealthBar.max_value = Global.player1MaxHealth
	elif Global.currentPlayer == "Player2":
		$PlayerHealthBar.max_value = Global.player2MaxHealth
		
func setHealthCurrentValue(player):
	if Global.currentPlayer == "Player":
		$PlayerHealthBar.max_value = Global.player1MaxHealth
		$PlayerHealthBar.value = Global.player1CurrHealth
	elif Global.currentPlayer == "Player2":
		$PlayerHealthBar.value = Global.player2CurrHealth
		$PlayerHealthBar.max_value = Global.player2MaxHealth

func _on_PausePanel_about_to_show():
	get_tree().set_pause(true)


func _on_PausePanel_popup_hide():
	get_tree().set_pause(false)


func _on_RestartLevelButton_pressed():
	Global.restart_level()


func _on_ResumeButton_pressed():
	$PausePanel.hide()


func _on_ExitButton_pressed():
	get_tree().quit()


func _on_RestartGame_pressed():
	Global.restart_game()


func _on_PlayerDeadPanel_about_to_show():
	get_tree().set_pause(true)


func _on_PlayerDeadPanel_popup_hide():
	get_tree().set_pause(false)


func _on_NextStageButton_pressed():
	$StageClearPanel/NextStageButton.hide()
	$StageClearPanel/FinalLabel.show()


func _on_MainMenu_pressed():
	Global.goto_scene("res://Scenes/Start Menu.tscn")
