extends "res://Scripts/Player.gd"

# Called when the node enters the scene tree for the first time.

func processAttack():
	if Input.is_action_just_pressed("attack"):
		animPlayer.travel("attack")
		isAttacking = true
	elif Input.is_action_just_pressed("special_attack"):
		animPlayer.travel("charged_attack")
		isAttacking = true

func get_input():
	xVelocity = 0
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		attackCount = 0
		yVelocity = jump_speed
		if(!isAttacking):
			animPlayer.travel("jump")
	else:
		if is_on_floor():
			if(!isAttacking):
				animPlayer.travel('idle')
			hasJumped = false
			jumpAttackCount = 0
		
		
	if Input.is_action_pressed('ui_right'):
		xVelocity += speed
		
		if(!hasJumped and !isAttacking):
			animPlayer.travel("run")
		
		if(!isAttacking):
			scale.x = -1 * scale.y
		
	if Input.is_action_pressed('ui_left'):
		xVelocity -= speed
		
		if(!hasJumped and !isAttacking):
			animPlayer.travel("run")
			
		if(!isAttacking):
			scale.x = 1 * scale.y
		

func _physics_process(delta):
	velocity.x = 0
	velocity.y = 0
	
	processAttack()
	if delayCount > delay:
		get_input()
		
	
	yVelocity += delta * GRAVITY
	velocity.x = xVelocity * facingState
	velocity.y = yVelocity
	velocity = move_and_slide(velocity, UP)
	
func _process(delta):
	if(Global.player2CurrHealth <= 0):
		self.get_node("Camera2D").clear_current()
		owner.get_node("Control").get_node("PlayerDeadPanel").popup()
		queue_free()
		
	elif Input.is_action_just_pressed("ui_cancel"):
		owner.get_node("Control").get_node("PausePanel").popup()
	
		
	if delayCount <= delay:
		delayCount += delta
		
	_physics_process(delta)
	
func damaged(damage):
	Global.player2CurrHealth -= damage
	
func recoverHp(amount):
	Global.player2CurrHealth += amount
	if(Global.player2CurrHealth > Global.player2MaxHealth):
		Global.player2CurrHealth = Global.player2MaxHealth
