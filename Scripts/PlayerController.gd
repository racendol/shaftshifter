extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var Player1 = $Player
onready var Player2 = $Player2
var player1Dead = false
var player2Dead = false
var firstSwitch = true
# Called when the node enters the scene tree for the first time.
func _ready():
	switch_player("Player", "Player2")
	firstSwitch = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Player2 != null:
		if Player2.isAttacking or player1Dead: 
			Global.player1Available = false
		else:
			Global.player1Available = true
	else:
		Global.player2Available = false
		
	if player2Dead:
		Global.player2Available = false
	else:
		Global.player2Available = true
		
	if Input.is_action_just_pressed("player1") and Global.currentPlayer != "Player" and Global.player1Available :
		switch_player("Player", "Player2")
		
	elif Input.is_action_just_pressed("player2") and Global.currentPlayer != "Player2" and Global.player2Available :
		switch_player("Player2", "Player")
		
	if Global.player1CurrHealth <= 0 and !player1Dead:
		player1Dead = true
		if  Global.player2CurrHealth > 0:
			switch_player("Player2", "Player")
			$Player.queue_free()
	
	if Global.player2CurrHealth <= 0 and !player2Dead:
		player2Dead = true
		if  Global.player1CurrHealth > 0:
			switch_player("Player", "Player2")
			$Player2.queue_free()
			
	if player1Dead and player2Dead:
		get_parent().get_node("Control").get_node("PlayerDeadPanel").popup()
		
		
func switch_player(playerToShow, playerToHide):
	var pos = get_node(playerToHide).position
	
	freeze_node(get_node(playerToShow), false)
	get_node(playerToShow).position = pos
	show_player(playerToShow, true)
	show_player(playerToHide, false)
	freeze_node(get_node(playerToHide), true)
	
	Global.currentPlayer = playerToShow
	if(!firstSwitch):
		$AudioStreamPlayer.play()
	

func show_player(player, show):
	player = get_node(player)
	player.get_node("Camera2D").current = show
	player.get_node("CollisionShape2D").disabled = !show
	player.get_node("Area2D").get_node("CollisionShape2D").disabled = true
	player.visible = show

func freeze_node(node, freeze):
	node.set_process(!freeze)
	node.set_physics_process(!freeze)
	node.set_process_input(!freeze)
	node.set_process_internal(!freeze)
	node.set_process_unhandled_input(!freeze)
	node.set_process_unhandled_key_input(!freeze)
