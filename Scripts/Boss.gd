extends "res://Scripts/Enemy.gd"


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (int) var state = 1
export (bool) var rageState = false
onready var rageTreshold = health/2
# Called when the node enters the scene tree for the first time.
#func _ready():
#	pass # Replace with function body.

func _process(delta):
	if(health <= rageTreshold and !rageState):
		state = 2
		damage = 100
		speed = 200
		permDelay = 8
		shootDelayStart = 8
		rageState = true
		animPlayer.play("idle_rage")
		position = Vector2(512, 150)
#		$AttackHitbox.get_node("CollisionShape2D").scale = Vector2(11.2, 0.955)
#		$AttackHitbox.get_node("CollisionShape2D").position = Vector2(-253, 25.689)
#		$AttackSight.get_node("CollisionShape2D").scale = Vector2(11.2, 0.955)
#		$AttackSight.get_node("CollisionShape2D").position = Vector2(-253.108, 25.689)
#		$FrontSight.get_node("CollisionShape2D").scale = Vector2(3, 1)
#		$FrontSight.get_node("CollisionShape2D").position = Vector2(-230, -20.496)
		
	if(health <= 0):
		Global.player2Unlocked = true
		Global.goto_scene("res://Scenes/Level 4.tscn")
	
	processMovement(delta)
	
func attack(delay):
	shootDelay = delay
	shootDelayStart = 0
	if(rageState):
		animPlayer.play("charged_attack")
	else:
		animPlayer.play("attack")
		
func processMovement(delta):
	velocity.x = 0
	velocity.y = 40*delta*1400
		
	if(shootDelayStart < shootDelay):
		shootDelayStart += delta
	
	if(shootDelayStart >= shootDelay):
		if(playerInRange):
			attack(permDelay)
			
		else:
			if(spottedPlayer):
				velocity.x += -facingState*speed
				animPlayer.play("run")
			elif (!rageState):
				animPlayer.play("idle")
			else:
				animPlayer.play("idle_rage")
				
			if(playerInBack):
				facingState *= -1
				scale.x = facingState * scale.y
				playerInBack = false
		
	move_and_slide(velocity, Vector2(0, -1))
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
