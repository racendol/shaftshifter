extends Area2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_body_entered(body):
	if('Player' in body.get_groups()):
		owner.get_node("AudioStreamPlayer").playing = false
		owner.get_node("Control").get_node("StageClearPanel").popup()
		owner.get_node("Control").get_node("StageClearPanel").get_node("AudioStreamPlayer").playing = true
