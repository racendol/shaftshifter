extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 1200
export (int) var jump_speed = -600
export (int) var attackCount = 0
export (int) var jumpAttackCount = 0
export (float) var delay = 0.3
export (int) var delayCount = 0
export (int) var facingState = 1
export (int) var xVelocity = 0
export (int) var yVelocity = 0
export (int) var attackDamage = 50
export (bool) var hasJumped = true
export (bool) var active = true
export (int) var health = 100
export (int) var maxHealth = 100
export (bool) var isAttacking = false
export (bool) var attackReset = false

const UP = Vector2(0,-1)

var velocity = Vector2()

var anim
var animPlayer

func getready():
	anim = self.get_node("AnimationTree")
	animPlayer = anim["parameters/playback"]
	animPlayer.start("idle")

func _ready():
	getready()

func processAttack():
	if delayCount >= 1:
		attackCount = 0
		
	if Input.is_action_just_pressed("attack") and delayCount >= delay:
		if (animPlayer.get_current_node() == 'run'):
			animPlayer.travel('slide')
			delay = 0.25
			delayCount = 0
			
		else:
			if(hasJumped):
				match jumpAttackCount:
					0, 3:
						animPlayer.travel("jump_attack1")
						jumpAttackCount = 1
						delay = 0.2
						delayCount = 0
					1:
						animPlayer.travel("jump_attack2")
						jumpAttackCount = 2
						delay = 0.2
						delayCount = 0
						
					2:
						animPlayer.travel("jump_attack3_ready")
						jumpAttackCount = 3
						GRAVITY = 2000
						delayCount = 0
						
			else:	
				match attackCount:
					0, 3:
						animPlayer.travel("attack1")
						attackCount = 1
						delay = 0.22
						delayCount = 0
					1:
						animPlayer.travel("attack2")
						attackCount = 2
						delay = 0.22
						delayCount = 0
						
					2:
						animPlayer.travel("attack3")
						attackCount = 3
						delay = 0.3
						delayCount = 0
					
	if is_on_floor() and jumpAttackCount == 3:
		animPlayer.travel("jump_attack3_drop")
		delay = 0.4
		GRAVITY = 1200
		jumpAttackCount = 0
		hasJumped = false

func get_input():
	xVelocity = 0
	if is_on_floor() and Input.is_action_just_pressed("ui_up"):
		attackCount = 0
		yVelocity = jump_speed
		animPlayer.travel("jump")
		hasJumped = true
	else:
		if is_on_floor():
			animPlayer.travel('idle')
			hasJumped = false
			jumpAttackCount = 0
			GRAVITY = 1200
		
		
	if Input.is_action_pressed('ui_right'):
		xVelocity += speed
		scale.x = 1 * scale.y
		if(!hasJumped):
			animPlayer.travel("run")
		facingState = 1
		
	if Input.is_action_pressed('ui_left'):
		xVelocity += speed
		scale.x = -1 * scale.y
		if(!hasJumped):
			animPlayer.travel("run")
		facingState = -1
		

func _physics_process(delta):
	velocity.x = 0
	velocity.y = 0
	
	processAttack()
	if delayCount > delay:
		get_input()
		
	
	yVelocity += delta * GRAVITY
	velocity.x = xVelocity * facingState
	velocity.y = yVelocity
	velocity = move_and_slide(velocity, UP)

func _process(delta):
	if(Global.player1CurrHealth <= 0):
		if !Global.player2Unlocked:
			self.get_node("Camera2D").clear_current()
			queue_free()
			owner.get_node("Control").get_node("PlayerDeadPanel").popup()
		
	elif Input.is_action_just_pressed("ui_cancel"):
		owner.get_node("Control").get_node("PausePanel").popup()
	
		
	if delayCount <= delay:
		delayCount += delta
		
	_physics_process(delta)

	
	
#	if velocity.y != 0:
#		animator.play("Jump")
#	elif velocity.x != 0:
#		animator.play("Walk")
#		if velocity.x > 0:
#			sprite.flip_h = false
#		else:
#			sprite.flip_h = true
#	else:
#		animator.play("Idle")
	


func _on_Area2D_body_entered(body):
	if('Enemy' in body.get_groups()):
		body.damaged(attackDamage)
		$AttackHit.play()
		
	if('Destructible' in body.get_groups()):
		body.destroy()
		
func damaged(damage):
	Global.player1CurrHealth -= damage
	$Damaged.play()
	
func recoverHp(amount):
	Global.player1CurrHealth += amount
	if(Global.player1CurrHealth > Global.player1MaxHealth):
		Global.player1CurrHealth = Global.player1MaxHealth
		
