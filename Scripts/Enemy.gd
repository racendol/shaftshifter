extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
export (int) var health = 100
export (int) var speed = 100
export (int) var damage = 50
export (int) var facingState = 1
export (int) var shootDelay = 0
export (int) var shootDelayStart = 0
export (int) var xVelocity = 0
export (int) var yVelocity = 0
export (bool) var spottedPlayer = false
export (bool) var playerInBack = false
export (bool) var playerInRange = false
export (int) var score = 200

var velocity = Vector2()
var permDelay = 0

onready var animPlayer = self.get_node("AnimationPlayer")


# Called when the node enters the scene tree for the first time.
func _ready():
	permDelay = shootDelay
	shootDelay = 0
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(health <= 0):
		queue_free()
		Global.playerCurrScore += score
		
	processMovement(delta)
	

func processMovement(delta):
	velocity.x = 0
	velocity.y = 40*delta*1400
		
	if(shootDelayStart < shootDelay):
		shootDelayStart += delta
	
	if(shootDelayStart >= shootDelay):
		if(playerInRange):
			attack(permDelay)
			
		else:
			if(spottedPlayer):
				velocity.x += -facingState*speed
				animPlayer.play("run")
			else:
				animPlayer.play("idle")
				
			if(playerInBack):
				facingState *= -1
				scale.x = facingState * scale.y
				playerInBack = false
		
	move_and_slide(velocity, Vector2(0, -1))

func damaged(damageAmount):
	health -= damageAmount


func _on_FrontSight_body_entered(body):
	if('Player' in body.get_groups()):
		spottedPlayer = true

func _on_FrontSight_body_exited(body):
	if('Player' in body.get_groups()):
		spottedPlayer = false

func _on_AttackSight_body_entered(body):
	if('Player' in body.get_groups()):
		playerInRange = true

func _on_AttackSight_body_exited(body):
	if('Player' in body.get_groups()):
		playerInRange = false

func attack(delay):
	shootDelay = delay
	shootDelayStart = 0
	animPlayer.play("attack")


func _on_BackSight_body_entered(body):
	if('Player' in body.get_groups()):
		if(shootDelayStart > shootDelay):
			spottedPlayer = true
			facingState *= -1
			scale.x = facingState * scale.y
		else:
			playerInBack = true

func _on_AttackHitbox_body_entered(body):
	if('Player' in body.get_groups()):
		body.damaged(damage)



