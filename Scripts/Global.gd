extends Node

var current_scene = null
var current_scene_path = null

var currentPlayer = "Player"
var player1Available = false
var player2Available = false
var player2Unlocked = false

var player1MaxHealth  = 100
var player1CurrHealth = 100
var startLevelPlayer1CurrHealth = 100
var startLevelPlayer1MaxHealth = 100

var player2MaxHealth  = 100
var player2CurrHealth = 100
var startLevelPlayer2CurrHealth = 100
var startLevelPlayer2MaxHealth = 100

var startLevelPlayerScore  = 0
var playerCurrScore = 0

func _ready():
	var root = get_tree().get_root()
	current_scene = root.get_child(root.get_child_count() - 1)

func restart_game():
	player1MaxHealth  = 100
	player1CurrHealth = 100
	player2MaxHealth  = 100
	player2CurrHealth = 100
	startLevelPlayer1CurrHealth = 100
	startLevelPlayer1MaxHealth = 100
	startLevelPlayer2CurrHealth = 100
	startLevelPlayer2MaxHealth = 100
	playerCurrScore = 0
	startLevelPlayerScore = 0
	player2Available = false
	player2Unlocked = false
	goto_scene("res://Scenes/Level 1.tscn")
	
func restart_level():
	player1MaxHealth  = startLevelPlayer1MaxHealth
	player1CurrHealth = startLevelPlayer1CurrHealth
	player2MaxHealth  = startLevelPlayer2MaxHealth
	player2CurrHealth = startLevelPlayer2CurrHealth
	playerCurrScore = startLevelPlayerScore
	goto_scene(current_scene_path)


func goto_scene(path):
	# This function will usually be called from a signal callback,
	# or some other function in the current scene.
	# Deleting the current scene at this point is
	# a bad idea, because it may still be executing code.
	# This will result in a crash or unexpected behavior.

	# The solution is to defer the load to a later time, when
	# we can be sure that no code from the current scene is running:

	call_deferred("_deferred_goto_scene", path)


func _deferred_goto_scene(path):
	# It is now safe to remove the current scene
	current_scene.free()

	# Load the new scene.
	var s = ResourceLoader.load(path)

	# Instance the new scene.
	current_scene = s.instance()
	
	current_scene_path = path

	# Add it to the active scene, as child of root.
	get_tree().get_root().add_child(current_scene)

	# Optionally, to make it compatible with the SceneTree.change_scene() API.
	get_tree().set_current_scene(current_scene)
