extends ProgressBar


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	max_value = get_parent().get_parent().get_node("Player").health
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	value = get_parent().health
